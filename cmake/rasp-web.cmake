set(CMAKE_SYSTEM_NAME Linux)
set(TOOLCHAIN_PREFIX arm-poky-linux-gnueabi)

set(SDK_SYSROOT /opt/poky/2.4+snapshot/sysroots/cortexa7hf-neon-vfpv4-poky-linux-gnueabi)
set(CROSS_PATH /opt/poky/2.4+snapshot/sysroots/x86_64-pokysdk-linux/usr/bin/arm-poky-linux-gnueabi)

# cross compilers to use for C and C++
set(CMAKE_C_COMPILER ${CROSS_PATH}/${TOOLCHAIN_PREFIX}-gcc)
set(CMAKE_CXX_COMPILER ${CROSS_PATH}/${TOOLCHAIN_PREFIX}-g++)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -march=armv7-a -mfloat-abi=soft --sysroot=${SDK_SYSROOT}" CACHE STRING "c++ flags")
set(CMAKE_C_FLAGS   "${CMAKE_C_FLAGS} -march=armv7-a -mfloat-abi=soft --sysroot=${SDK_SYSROOT}" CACHE STRING "c flags")
set(CMAKE_EXE_LINKER_FLAGS "--sysroot=${SDK_SYSROOT}")

# target environment on the build host system
#   set 1st to dir with the cross compiler's C/C++ headers/libs
set(CMAKE_FIND_ROOT_PATH ${SDK_SYSROOT})

set(ENV{PKG_CONFIG_SYSROOT_DIR} ${SDK_SYSROOT})
set(ENV{PKG_CONFIG_PATH} ${SDK_SYSROOT}/usr/lib/pkgconfig)

# modify default behavior of FIND_XXX() commands to
# search for headers/libs in the target environment and
# search for programs in the build host environment
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

set(THREADS_PTHREAD_ARG 1)
