#ifndef MANAGEGPIO_H
#define MANAGEGPIO_H

#include <stdbool.h>

int checkAval(char* pin);
char* concat(char* extra);
char* setPath(char* pin, int opt);
int pinMode (char* pin, char* MODE);
int digitalWrite(char* pin, char* value);
void digitalRead(char* pin);
int blink(char* pin, int* freq, int* duration);

#endif // MANAGEGPIO_H
