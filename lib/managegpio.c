#include "../include/managegpio.h"
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

/*
* Global variables
*/
FILE *fp;
char gpioPath[] = "/sys/class/gpio/gpio";
char pinDir[] = "/direction";
char pinVal[] = "/value";

/*
* Permite generar un blink (establecer y desestablecer un valor binario) en
*/
int checkAval(char* pin)
{
  char* gpio_dir = setPath(pin,1);

  /*
  * Return 0 if file not exits
  * 1 if in
  * -1 if out
  */
  if( access( gpio_dir, F_OK ) != -1 ) {
    return 1; //TODO: GET IN OR OUT
    return -1;
  }
  return 0;
}

/*
* Generates path to specific pinIO
*/
char* concat(char* pin)
{
  char* midPath;
  /* make space for the new string (should check the return value ...) */
  midPath = malloc(strlen(gpioPath)+strlen(pin)+1);
  strcpy(midPath, gpioPath); /* copy name into the new var */
  strcat(midPath, pin);
  return midPath;
}

/*
* Sets path for values or direction.
*/
char* setPath(char* pin, int opt)
{
  char* midPath;
  midPath = concat(pin);
  char* fullPath;

  /*
  * opt 1 for mode, 2 for value
  */
  switch (opt) {
    /* To set direction */
    case 1:
    {
      fullPath = malloc(strlen(midPath)+strlen(pinDir)+1);
      strcpy (fullPath,midPath);
      strcat (fullPath,pinDir);
    }
    /* To set value */
    case 2:
    {
      fullPath = malloc(strlen(midPath)+strlen(pinVal)+1);
      strcpy (fullPath,midPath);
      strcat (fullPath,pinVal);
    }
  }
  return fullPath;
}

/*
* Permite establecer entrada el (modo/salida) de un pin especı́fico (número de
* pin/gpio).
*/
int pinMode(char* pin, char* MODE)
{
  if(checkAval(pin) != 0)
  {
    /*
    * First export pin
    */
    char blockPin[] = "/sys/class/gpio/export";

    fp = fopen( blockPin, "w" );
    if(fp == NULL){
      printf("file not found.\n");
      return -1;
    }
    fputs(pin, fp); //writes pin on use
    fclose(fp);

    /*
    * Then to set mode
    */
    char* fullPath = setPath(pin,1);
    fp = fopen( fullPath, "w" );
    if(fp==NULL){
      printf("file not found.\n");
      return -1;
    }

    char md[] = "out";
    char in[] = "in";
    if(*MODE == 1){
      strcpy(md,in);
    }
    if(fp!=NULL){
      fputs(md, fp); //writes mode
      fclose(fp);
      return 0;
    }
  }
  printf("Pin already taken. Try with changePinMode(char* pin, char* MODE)\n");
  return -1;
}

/*
* Permite escribir un valor de 0 o 1 en el pin especı́fico configurado como
* salida.
*/
int digitalWrite(char* pin, char* value)
{
  if(checkAval(pin) == -1)
  {
    char* fullPath = setPath(pin,2);
    fp = fopen( fullPath,"w" );

    if(fp!=NULL){
      fputs(value, fp);
      fclose(fp);
      return 0;
    }
  }
  return -1;
}

/*
 * Permite leer el estado (0,1) de un pin digital.
 */
void digitalRead(char* pin)
{
  if(checkAval(pin) == 1)
  {
    char buff[2];
    char* fullPath = setPath(pin,2);
    fp = fopen( fullPath,"r" );

    if(fp!=NULL){
      fscanf(fp, "%c", buff);
      fclose(fp);
    }

    printf("%s\n",buff);
    //if{buff}

    //return buff;
  }
}

/*
* Permite generar un blink (establecer y desestablecer un valor binario)
* en un pin a una frecuencia determinada, por un tiempo de duración
* determinado.
*/
int blink(char* pin, int* freq, int* duration)
{

  int way = checkAval(pin); //get avaibility

  if(way == 1) // set to input
  {
    return -1;
  }

  char mode = '1';
  if(way == 0)  //no set
  {
    pinMode(pin, &mode);
  }

  size_t dur = 0; //acumulado de tiempo
  char lset = '1';  //led state
  char zero = '0';
  char one = '1';

  /*
  * 0. frecuencia = encendido por segundo
  */

  int manyCalls = 2 * *freq;
  int slp = 1000/manyCalls;

  if(*duration < slp)
  {
    return -1;
  }

  /*
  * 1. get path
  */
  char* fullPath;
  fullPath = setPath(pin, 2);

  /*
  * 2. duración = ms
  */
  while(dur <= *duration)
  {
    digitalWrite(pin,&lset);
    if(lset == one){
      strcpy(&lset,&zero);
    }else{
      strcpy(&lset,&one);
    }
    dur += slp;
    sleep(slp);
  }
  return 0;
}
