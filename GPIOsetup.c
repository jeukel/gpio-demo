/*
 * GPIOsetup.c
 *
 * Copyright 2018 Daniel Jenkins <jeukel@Galago>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include <stdio.h>
#include <pthread.h>
#include <string.h>
#include <stdlib.h>

#include "include/managegpio.h"

#define NUM_THREADS 3

int btime = 5000;
int freq = 3;
char sequence[] = "01110100101001001";

char pin1[] = "1";
char pin2[] = "2";
char pin3[] = "3";
char* readPin;


void *binSequen(void *threadid)
{
  // manda secuencia binaria a pin1
  for(int c = 0; c<strlen(sequence);c++)
  {
    digitalWrite(pin1, &sequence[c]);
  }

  pthread_exit(NULL);
}

void *staticBlink(void *threadid)
{
  //blink para pin2 en btime segundos
  blink(pin2, &freq, &btime);

  pthread_exit(NULL);
}

void *pinRead(void *threadid)
{
  // imprime valor de pin3
  printf("Valor pin3: \n");
  //readPin = digitalRead(pin3);
  //printf("%f \n",readPin);

  pthread_exit(NULL);
}

void GPIOsetup()
{
  pthread_t threads[NUM_THREADS];

  pinMode(pin1, "out");
  pinMode(pin2, "out");
  pinMode(pin3, "in");

  int rc1, rc2, rc3;
  size_t i = 1;

  rc1 = pthread_create(&threads[i], NULL, binSequen, (void *)i);
  i++;
  rc2 = pthread_create(&threads[i], NULL, staticBlink, (void *)i);
  i++;
  rc3 = pthread_create(&threads[i], NULL, pinRead, (void *)i);

  if (rc1 || rc2 || rc3) {
    printf("Error:unable to create a thread.\n");
    exit(-1);
  }

  pthread_exit(NULL);
}

/*
* Configura dos gpios de la tarjeta como salida y uno como entrada.
* La aplicación escribe valores binarios (0,1) a uno de los gpio configurados
* como salidas y en el otro establece un "blink" a una frecuencia determinada
* por una duración de 5 s.
* Adicionalmente, lee el valor del pin configurado como entrada y lo presenta
* en pantalla.
*/
int main()
{
  GPIOsetup();
  return 0;
}
