# Tutorial: Yocto Project Parte II


<div style="text-align: right"> **Daniel Jenkins** </div>
<div style="text-align: right"> 200579636 </div>

```
Hardware: Raspberry Pi 3 model B v1.2
Chip: BCM2837RIFBG

```

## Preguntas Tutorial

**6. ¿Por qué no deberı́a verse ’Hello World!’ ? ¿Qué quiere decir la salida de la aplicación?**

El programa fue compilado para una arquitectura distinta a la del sistema de compilación.

**9. ¿Cuál es la salida en este caso? ¿Qué quiere decir?**

Marca la falta de una librería del sistema. Esto implica que el binario no fue compilado dentro del proyecto de yocto por lo que no agregó dicha librería al build del sistema.

## Investigación
**1. ¿Qué pasos debe seguir antes de escribir o leer de un puerto de entrada/salida general (GPIO)?**

Aplicando el método de uso nativo de lectura y escritura por medio del kernel de Linux, leer el archivo "export" con el fin de ver si algún pin está activo. Segundo, si es afirmativo, ver en que modalidad está (i/o). En su generalidad, los pines a usar no se estén usando previamente en algún modo. Igualmente, algunos pines pueden estar trabajando en conjunto por lo que es recomendable verificar sus archivos de configuración.

**2. ¿Qué comando podrı́a utilizar, bajo Linux, para escribir a un GPIO especı́fico?**

<div style="text-align: center"> `echo "1" > /sys/class/gpio/gpio4/value` </div>

## Generación de Makefiles
```
autoreconf --install
```

## Comprobación de contrucción
```
mkdir -p build/usr
./configure --prefix=/home/"$USER"/cmake-demo/build/usr/
make
make install
```

## Generador de librería
```
make distcheck
```
