#
#********************************** INTRO: **********************************

#   Se espera que el proyecto sea desarrollado dentro de la misma carpeta en
#   que se compile este proyecto.
#   El script está basado en el hecho de que usted tiene ya instalado el 
#   toolchain elaborado con yocto en conjunto con el de meta-raspberry 
#   YOCTO_TOOLCHAIN se partirá de ese directorio para generar todo el compilado
#   cruzado.
#

#!/bin/bash

YOCTO_TOOLCHAIN=../cmake/rasp-web.cmake
EVM_SETUP=/opt/poky/2.4+snapshot/environment-setup-cortexa7hf-neon-vfpv4-poky-linux-gnueabi
HERE=`readlink -f .`

#INSTALL CMAKE IF NOT AVAILABLE (DEBIAN DERIVATIVES)
sudo apt install cmake

## UNCOMMENT IF SOURCE WASN'T ADQUIRE VIA THE GITLAB REPO.
## git clone git@gitlab.com:jeukel/gpio-demo.git

. $EVM_SETUP
echo $CXX # SOMETHING LIKE: arm-poky-linux-gnueabi-g++... It's fine.

mkdir -p build/usr
cd build
cmake ../ -DFAB:STRING=po -DCMAKE_TOOLCHAIN_FILE=$YOCTO_TOOLCHAIN -DCMAKE_INSTALL_PREFIX:PATH=$HERE/build/usr
make

